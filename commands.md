## Commands available inside the blockchain node container

# `agevote-setup`

The entrypoint script is waiting for the `agevote-setup` command to be run once.
When executed all the usefull information from key generation will be displayed.

The secret phrase will be used as an environment variable for the sidecar container and the last 3 keys (Peer ID, Sr25519 and Ed25519) must be transmitted to AGEPoly before proceeding to Part II. Once transmitted avoid losing the keys (stored in the blockchain volume).

If you run the script again it will display the same output.

Example output:
```
Secret phrase       : "galaxy observe clay cruel pupil atom early edit hurdle fever hero repeat"
Peer ID             : 12D3KooWCmURd5Fneq2u5DaqqmSMeYzSETMnpTRqRVWWpxPFEKLb
Sr25519 public key  : 5Fe8EGmCypU8uJ1nB9hycDowSvXiZehzU6MDz9oCov9i2Cpv
Edr25519 public key : 5H5VfreQNPe7tev6YdwWjXLVWBuKxi9aSGYDgwNnok1WGHwU
```

# `agevote-reset-blockchain`

Blockchain nodes must agree on the first block (called *genesis block*). When a new member is added, this genesis block must be modified. For now such a modification cannot be done live and the whole blockchain database must be reset.

For convenience this script will halt the blockchain executable, reset the database (**the keys stay unchanged**) and restart the blockchain executable.

This command doesn't output anything but the restart process is fully visible on the container logs.

# `agevote-wipe-all`

**WARNING**: this should not be used unless asked specifically by AGEPoly.

This script will delete everything in the /data directory including the full state of the blockchain including your keys and the node keystore. Reseting only the blockchain is done with the command above.