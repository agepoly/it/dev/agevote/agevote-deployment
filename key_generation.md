# [OBSOLETE Documentation // Not required to deploy]

### Secret phrase and the two derived pairs

One key pair can be generated with the following subcommand of the node executable:
```bash
./node-agevote key generate --scheme Sr25519 --password-interactive

# Example output fot the account "Alfred" with password "insecure"
# Secret phrase:       almost siren mask oven topic movie wage modify match fatal rabbit lounge
#   Network ID:        substrate
#   Secret seed:       0x562aaac10d21daf14e72dc81e5bd43f811d19159483423d8a350bc7109413bfa
#   Public key (hex):  0x6472afb72bf05bf489687612f7b725f5138d2008ef49ac7178ad9f1441d31029
#   Account ID:        0x6472afb72bf05bf489687612f7b725f5138d2008ef49ac7178ad9f1441d31029
#   Public key (SS58): 5ELQiQBaXhQ5P9LHEhUsfXdUQEujBKmUJqk5fok52eyNjBPC
#   SS58 Address:      5ELQiQBaXhQ5P9LHEhUsfXdUQEujBKmUJqk5fok52eyNjBPC
```

The key is derived to generate a public key in the Sr25519 scheme (note the `--scheme Sr25519`) and another one on Ed255519 with:

```bash
./node-agevote key inspect --password-interactive --scheme Ed25519 "pig giraffe ceiling enter weird liar orange decline behind total despair fly"

# Example output fot the account "Alfred" with password "insecure"
# Secret phrase:       almost siren mask oven topic movie wage modify match fatal rabbit lounge
#   Network ID:        substrate
#   Secret seed:       0x562aaac10d21daf14e72dc81e5bd43f811d19159483423d8a350bc7109413bfa
#   Public key (hex):  0x8d70a258c1eb2f8d3696fea446cc2416fcdaec0a63a1af1d4b1a1710f65aa859
#   Account ID:        0x8d70a258c1eb2f8d3696fea446cc2416fcdaec0a63a1af1d4b1a1710f65aa859
#   Public key (SS58): 5FGA4up8u1AZmr3wR6Nirg5KVb34K2oQfe6HRAa2vQPaMyqE
#   SS58 Address:      5FGA4up8u1AZmr3wR6Nirg5KVb34K2oQfe6HRAa2vQPaMyqE
```

Example for the "Alice" key (derived from the phrase "//Alice") :
Sr25519 : 5ELQiQBaXhQ5P9LHEhUsfXdUQEujBKmUJqk5fok52eyNjBPC used by Aura & AGEVoté
Ed25519 : 5FGA4up8u1AZmr3wR6Nirg5KVb34K2oQfe6HRAa2vQPaMyqE used by Grandpa

### Chain specification creation

Both keys need to be transmitted to AGEPoly IT manager to create the *Chain Specifications*. This is a large JSON file that contains the metadata to create the initial state of the blockchain, this includes the big binary blob that creates the validation rules in WebAssembly byte code. This JSON file can be converted to a "raw" format (the key value store of the database).
The commands are :
- `./node-agevote build-spec --disable-default-bootnode --chain local > agevoteSpec.json`
- edit agevoteSpec.json to add the freshly generated keys to the respective modules
- `./node-agevote build-spec --chain=agevoteSpec.json --raw --disable-default-bootnode > agevoteSpecRaw.json`

### First node

Start the first node:
`./node-agevote --base-path ./custom/alfred --chain agevoteSpecRaw.json --port 30333 --ws-port 9944  --validator --rpc-methods Safe --name Alfred --password-interactive`

Insert the Sr25519 type key for Aura into the first node's keystore: (NB you can use the secret phrase for the `--suri` but also be a file)
`./node-agevote key insert --base-path ./custom/alfred --chain agevoteSpecRaw.json --scheme Sr25519 --suri <your-secret-key> --password-interactive --key-type aura`

Similarly with the Ed25519 scheme for the Grandpa key:
`./node-agevote key insert --base-path ./custom/alfred --chain agevoteSpecRaw.json --scheme Ed25519 --suri <your-secret-key> --password-interactive --key-type gran`

### Second node

Start the second node:
`./node-agevote --base-path ./custom/barbie --chain agevoteSpecRaw.json --port 30334 --ws-port 9945  --validator --rpc-methods Safe --name Barbie --password-interactive --bootnodes /ip4/<IP-of-the-first-node>/tcp/30333/p2p/<node-identity-in-the-first-node-logs> \`

Insert both keys:
`./node-agevote key insert --base-path ./custom/barbie --chain agevoteSpecRaw.json --scheme Sr25519 --suri <the-second-secret-key> --password-interactive --key-type aura`
and
`./node-agevote key insert --base-path ./custom/barbie --chain agevoteSpecRaw.json --scheme Ed25519 --suri <the-second-secret-key> --password-interactive --key-type gran`

A note on block production and finalization :
- to author blocks the node need its Aura key installed and be the one in charge of authoring the current block
- to finalize blocks the blockchain need 2/3 of its authority nodes to vote on the longest chain